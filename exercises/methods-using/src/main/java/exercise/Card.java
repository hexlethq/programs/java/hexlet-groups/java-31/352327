package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String a = "*";
        System.out.println(a.repeat(starsCount) + cardNumber.substring(cardNumber.length() - 4));
        // END
    }
}
