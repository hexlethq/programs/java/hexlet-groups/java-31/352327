package exercise;

class App {
    public static void numbers() {
        // BEGIN
        int d1 = 8 / 2;
        int d2 = 100 % 3;
        System.out.println(d1 + d2);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        String does = " works";
        String where = " on ";
        String where2 = "JVM";

        System.out.println(language + does + where + where2);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        String e1 = " ";
        System.out.println(soldiersCount + e1 + name);
        // END
    }
}
